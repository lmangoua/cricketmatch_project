/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wtech.sport;

/**
 *
 * @author lmangoua
 * Date: 07/09/2016
 */
public abstract class Player {
    
    //Instance declaration
    private String name;
    private String surname;
    
    //Constructor
    public Player(String name, String surname) {
        
        //only use "this" when u want to call a variable within the same class ur working on, 
        //but i think even with global variables u can also use it
        this.name = name;
        this.surname = surname;
    }
    
    //Getters
    public String getName() {
        return name;
    }
    
    public String getSurname() {
        return surname;
    }
    
    public abstract void Play();
    
}
