/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wtech.sport.cricket;

/**
 *
 * @author lmangoua
 */
public class Keeper extends Cricketer {
    
    //Instance declaration
    private int totalCatches, currentCatches;
            
    //Constructor
    public Keeper(String name, String surname, int highestScore, int totalRuns, int noOfInnings, int totalCatches) {
        
        super(name, surname, highestScore, totalRuns, noOfInnings);
        this.totalCatches = totalCatches;
    }
    
    //Constructor for Keepers who haven't played before
    public Keeper(String name, String surname) {
        
        this(name, surname, 0, 0, 0, 0); //initialisation
    }
    
    public void keepWicket() {
        
        int randomNum; //value in range [0, 11[ or [0, 11) with 11 excluded
        
        randomNum = 0 + (int)(Math.random() * (11 - 0));
        currentCatches = randomNum;
        
        System.out.println(super.getName() + " " + super.getSurname() + " took " + currentCatches + " catch(es) behind the stumps.");
        
        totalCatches += currentCatches; 
    }
    
    //override from "Cricketer"
    public void printDetails() {
        
        super.printDetails(); //Call printDetails() from "Cricketer"
        
        System.out.println("And took: " + currentCatches + " today.");
        System.out.print("Total catches is now: " + totalCatches);
    }
    
    //Overridin abstract method Play() from superclass "Player" and "Cricketer"  
    public void Play(){
         
        super.bat();
        keepWicket();
    } 
}
