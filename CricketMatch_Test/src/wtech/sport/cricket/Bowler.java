/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wtech.sport.cricket;

/**
 *
 * @author lmangoua
 */
public class Bowler extends Cricketer {
    
    //Instance declaration
    private int totalWickets, currentWickets;
            
    //Constructor
    public Bowler(String name, String surname, int highestScore, int totalRuns, int noOfInnings, int totalWickets) {
        
        super(name, surname, highestScore, totalRuns, noOfInnings); //from the superclass "Cricketer"
        this.totalWickets = totalWickets;
    }
    
    //Constructor for Bowlers who haven't played before
    public Bowler(String name, String surname) {
        
        this(name, surname, 0, 0, 0, 0); //initialisation
    }
    
    public void bowl() {
        
        int randomNum; //value in range [0, 3[ or [0, 3) with 3 excluded
        
        randomNum = 0 + (int)(Math.random() * (3 - 0)); //int randomNum = min + (int)(Math.random() * ((max - min) + 1)) in case of max "inclusive"
        currentWickets = randomNum;
        
        System.out.println(super.getName() + " " + super.getSurname() + " took " + currentWickets + " wicket(s).");
        
        totalWickets += currentWickets;
    }
    
    //override from "Cricketer"
    public void printDetails() {
        
        super.printDetails(); //Call printDetails() from "Cricketer"
        
        System.out.println("Wickets taken today: " + currentWickets + ".");
        System.out.print("Total wickets is now: " + totalWickets);
    }
    
    //Overridin abstract method Play() from superclass "Player" and "Cricketer"  
    public void Play(){
         
        bat();
        bowl();
    }
}
